require 'additionalFunction'

local bullets = require 'bullets'
local player = {}

player.position_x = 100
player.position_y = 550
player.speed_x = 300

player.width = 50
player.height = 50

player.image = love.graphics.newImage('images/player.png')

player.lives = 5
player.shot_cooldown = 0


local scaleX, scaleY = getImageScaleForNewDimensions( player.image, player.width, player.height )

function player.update( dt )
    if love.keyboard.isDown( "right" ) and 
            player.position_x < ( love.graphics.getWidth() - player.width ) then
        player.position_x = player.position_x + ( player.speed_x * dt )
    end
    if love.keyboard.isDown( "left" )  and player.position_x > 0 then
        player.position_x = player.position_x - ( player.speed_x * dt )
    end
    if love.keyboard.isDown( " " ) and player.shot_cooldown <= 0 then
	bullets.fire( player )
	player.shot_cooldown = 60
    end
    if player.shot_cooldown > 0 then
	player.shot_cooldown = player.shot_cooldown - 1
    end
end

function player.draw()
    love.graphics.draw(player.image,
                       player.position_x,
                       player.position_y, rotation, scaleX, scaleY )
end

return player
