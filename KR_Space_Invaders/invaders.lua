require 'additionalFunction'

local invaders = {}

invaders.rows = 5
invaders.columns = 9

invaders.top_left_position_x = 50
invaders.top_left_position_y = 50

invaders.invader_width = 40
invaders.invader_height = 40

invaders.horizontal_distance = 20
invaders.vertical_distance = 30

invaders.current_speed_x = 50
invaders.allow_overlap_direction = 'right'
invaders.speed_x_increase_on_destroying = 10

invaders.current_level_invaders = {}

local initial_speed_x = 50
local initial_direction = 'right'

invaders.enemy_image = love.graphics.newImage('images/invader.png')


local scaleX, scaleY = getImageScaleForNewDimensions( invaders.enemy_image, invaders.invader_width,
    invaders.invader_height )

function invaders.destroy_invader( row, invader )
    invaders.current_level_invaders[row][invader] = nil
    local invaders_row_count = 0
    for _, invader in pairs( invaders.current_level_invaders[row] ) do
        invaders_row_count = invaders_row_count + 1
    end
    if invaders_row_count == 0 then
        invaders.current_level_invaders[row] = nil
    end
    if invaders.allow_overlap_direction == 'right' then
        invaders.current_speed_x = invaders.current_speed_x + invaders.speed_x_increase_on_destroying
    else
        invaders.current_speed_x = invaders.current_speed_x - invaders.speed_x_increase_on_destroying
    end
 end

function invaders.descend_by_row_invader( single_invader, bottom_wall, gamestate )
    single_invader.position_y = single_invader.position_y + invaders.vertical_distance / 2
    if single_invader.position_y + invaders.invader_height >= bottom_wall.position_y then
        gamestate.state = 'lose_life'
    end
end

function invaders.descend_by_row( bottom_wall, gamestate )
    for _, invader_row in pairs( invaders.current_level_invaders ) do
        for _, invader in pairs( invader_row ) do
            invaders.descend_by_row_invader( invader, bottom_wall, gamestate )
        end
    end
end

function invaders.new_invader(position_x, position_y, invader_image_no )
    local invader_image
    if not invaders.enemy_image then
        invader_image = invaders.enemy_image
    else
        invader_image = invaders.enemy_image
    end
    return ({position_x = position_x,
             position_y = position_y,
             width = invaders.invader_width,
             height = invaders.invader_height,
             image = invader_image})
end

function invaders.new_row( row_index, invader_image_no )
    local row = {}
    for col_index=1, invaders.columns - (row_index % 2) do
        local new_invader_position_x = invaders.top_left_position_x + invaders.invader_width * (row_index % 2) 
            + (col_index - 1) * (invaders.invader_width + invaders.horizontal_distance)
        local new_invader_position_y = invaders.top_left_position_y
            + (row_index - 1) * (invaders.invader_height + invaders.vertical_distance)
        local new_invader = invaders.new_invader( new_invader_position_x, 
                                                  new_invader_position_y, invader_image_no )
        table.insert( row, new_invader )
    end
    return row
end

function invaders.construct_level( invader_image_no )
    invaders.no_more_invaders = false
    invaders.allow_overlap_direction = initial_direction
    invaders.current_speed_x = initial_speed_x
    for row_index=1, invaders.rows do
        local invaders_row = invaders.new_row( row_index, invader_image_no )
        table.insert( invaders.current_level_invaders, invaders_row )
    end
    invaders.allow_decrease_player_lives = true
end

function invaders.clear_current_level()
    for i in pairs( invaders.current_level_invaders ) do
        invaders.current_level_invaders[i] = nil
    end
end

function invaders.draw_invader( single_invader)
    love.graphics.draw(single_invader.image,
                       single_invader.position_x,
                       single_invader.position_y, rotation, scaleX, scaleY )
end

function invaders.draw()
    for _, invader_row in pairs( invaders.current_level_invaders ) do
        for _, invader in pairs( invader_row ) do
            invaders.draw_invader( invader )
        end
    end
end

function invaders.update_invader( dt, single_invader )
    single_invader.position_x = single_invader.position_x + invaders.current_speed_x * dt
end


function invaders.update( dt )
    local invaders_rows = 0
    for _, invader_row in pairs( invaders.current_level_invaders ) do
        invaders_rows = invaders_rows + 1
    end
    if invaders_rows == 0 then
        invaders.no_more_invaders = true
    else
        for _, invader_row in pairs( invaders.current_level_invaders ) do
            for _, invader in pairs( invader_row ) do
                invaders.update_invader( dt, invader )
            end
        end
    end
end

return invaders
