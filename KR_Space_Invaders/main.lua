local player     = require 'player'
local invaders   = require 'invaders'
local walls      = require 'walls'
local collisions = require 'collisions'
local bullets    = require 'bullets'
local gamestate  = require 'gamestate'

local current_level = 1
local debug = false

function love.load()
    invaders.construct_level( current_level )
    walls.construct_level()
end

function love.keyreleased( key, code )
    if gamestate.state == 'start_game' then
        if key == 'return' then
            gamestate.state = 'game'
        elseif key == 'escape' then
            love.event.quit()
        end
    elseif gamestate.state == 'game' then
	if key == 'space' then
            bullets.fire( player )
        elseif key == 'escape' then
            gamestate.state = 'game_paussed'
        elseif key == 's' then
            debug = not debug
        end
    elseif gamestate.state == 'game_paussed' then
        if key == 'return' then
            gamestate.state = 'game'
        elseif key == 'escape' then
            love.event.quit()
        end
    elseif gamestate.state == 'win_round' then
        if key == 'return' then
            current_level = current_level + 1
            bullets.clear_current_level()
            invaders.construct_level( current_level )
            gamestate.state = 'game'
        end
    elseif gamestate.state == 'lose_life' then
        if key == 'return' then
            player.lives = player.lives - 1
            invaders.clear_current_level()
            invaders.construct_level( current_level )
            gamestate.state = 'game'
        end
    elseif gamestate.state == 'end_game' then
        if key == 'return' then
            player.lives = 5
            invaders.clear_current_level()
            current_level = 1
            invaders.construct_level( current_level )
            gamestate.state = 'game'
        elseif key == 'escape' then
            love.event.quit()
        end
    end
end

function love.draw()
    if gamestate.state == 'start_game' then
        love.graphics.printf('WELCOME to Space Invaders\n Press Return to start the game\n Press ESC to exit\n README - source the game code leanred and based upon', 300, 300, 300, 'center')
    elseif gamestate.state == 'game' then
        player.draw()
        invaders.draw()
        bullets.draw()
    elseif gamestate.state == 'game_paussed' then
        love.graphics.printf('PAUSE\n Press Return to unpause\n Press ESC to exit game', 300, 300, 200, 'center')
    elseif gamestate.state == 'lose_life' then
        if player.lives >= 1 then
            love.graphics.printf('You died!\n Press Return to continue', 300, 300, 200, 'center')
        else
            gamestate.state = 'end_game'
        end
    elseif gamestate.state == 'win_round' then
        love.graphics.printf('You win!\n Press Return to continue', 300, 300, 200, 'center')
    elseif gamestate.state == 'end_game' then
        love.graphics.printf('The End!', 300, 300, 200, 'center')
    end
end

function love.update( dt )
    if gamestate.state == 'game' then
        player.update( dt )
        collisions.resolve_collisions( walls, invaders, bullets, gamestate )
        invaders.update( dt )
        bullets.update( dt )
        if invaders.no_more_invaders then
            gamestate.state = 'win_round'
        end
    end
end
